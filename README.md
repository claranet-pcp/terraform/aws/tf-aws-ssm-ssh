Terraform SSH SSM module
-----

Creates an SSM document to allow password-less `sudo` access to Linux instances.

Make https://github.com/claranet/ssha work in your terraform project by using this module.

This module creates a suitable SSM document which will create people a login on
a machine with password-less `sudo` access.  This is much better than the bad
old days, when users accounts were managed through Puppet, or even worse created
at image build time.

By default, any user with `ssm:SendCommand` permissions can create themselves a
shell account with full `sudo` access on any SSM managed EC2 instance once this
module has been included.  On most customers, we  also restrict SSH access by
IP, so you may need firewall changes as well.

It is also possible to use this module to allow restricted SSH access, so that
IAM users only have access to EC2 instances with a specific tag.


## Terraform version compatibility

| Module version | Terraform version |
|----------------|-------------------|
| 1.5.0, 2.x.x   | 0.12.x            |
| <= 1.4.0       | 0.11.x            |


Usage
-----

##### Plain example

```js
module "ssm_ssh" {
  source = "../modules/tf-aws-ssm-ssh"
}

module "bastion_instance_profile" {
  source = "../modules/tf-aws-iam-instance-profile"

  name = "${var.envtype}_${var.brand}_bastion_iam_profile_default"

  ssm_managed = "1"
}

module "bastion" {
  source = "../modules/tf-aws-asg"

  name                        = "bastion-${var.envtype}"
  envname                     = "${var.brand}"
  service                     = "bastion"
  ami_id                      = "${var.aws_base_ami}"
  instance_type               = "t2.nano"
  iam_instance_profile        = "${module.bastion_instance_profile.profile_id}"
  security_groups             = ["${module.common_sg.security_group_id}", "${module.bastion_sg.security_group_id}"]
  associate_public_ip_address = "true"
  user_data                   = "${data.template_file.bastion_user_data.rendered}"
  subnets                     = "${module.vpc.public_subnets}"
  key_name                    = "${var.ssh_keyname}"

  extra_tags = [
    {
      key                 = "envtype"
      value               = "${var.envtype}"
      propagate_at_launch = true
    },
    {
      key                 = "envname"
      value               = "${var.envtype}"
      propagate_at_launch = true
    },
    {
      key                 = "brand"
      value               = "${var.brand}"
      propagate_at_launch = true
    },
  ]
}
```
##### Restricted Access example

The following example shows how to add an SSM document that allows restricted
SSH access for an IAM group. The [ssha](https://github.com/claranet/ssha)
documentation will demonstrate how to configure ssha to use a different SSM
document depending on the IAM users group.

```js
# Create a group that will be using ssha

resource "aws_iam_group" "developers" {
  name = "developers"
}

# Allow this group to SSH into some EC2 instances

module "ssm_ssh_developers" {
  source = "../modules/tf-aws-ssm-ssh"

  # Use a unique document name for each restricted group
  ssm_document_name = "add-ssh-key-${aws_iam_group.developers.name}"

  # This will add a policy to the group to allow ssha to work
  add_ssha_group_policy = true
  group_name            = "${aws_iam_group.developers.name}"

  # This tag must be on the allowed EC2 instances
  required_ec2_tag = "SSH:developers"
}

# Add the tag to EC2 instances to allow SSH access

module "bastion" {
  ...

  extra_tags = [
    {
      key                 = "${module.ssm_ssh_developers.required_ec2_tag}"
      value               = ""
      propagate_at_launch = true
    },
  ]

  ...
}
```


Variables
---------
_Variables marked with **[*]** are mandatory._

 - `ssm_document_name` - The name of SSM document to create. **[*]**
 - `add_ssha_group_policy` - Whether to add a policy to the group that allows SSHA to work. **[*]**
 - `group_name` - Group name, optional, for the policy. [Default: _blank_]
 - `required_ec2_tag` - Tag key, optional, restricts access to EC2 instances with this tag. [Default: _blank_]
 - `key_cleanup` - Whether to enable an SSM task to remove the authorized_key files from prefixed users after one hour. [Default: false]
 - `key_cleanup_ec2_tag` - Tag to look for when running the cleanup SSM task. [Default: SSHA_Cleanup]
 - `user_prefix` - Username prefix, optional, enforces usernames to have this prefix before creation. [Default: _blank_]


Outputs
-------

 - `required_ec2_tag` - The tag to add to EC2 instances to allow access.
 - `ssha_policy` - The policy JSON required for using SSHA.
