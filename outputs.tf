output "required_ec2_tag" {
  description = "Add this tag to EC2 instances to allow access"
  value       = var.required_ec2_tag
}

output "ssha_policy" {
  description = "The policy JSON required for using SSHA."
  value       = join("", data.aws_iam_policy_document.ssha.*.json)
}
