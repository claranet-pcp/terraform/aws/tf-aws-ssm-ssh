variable "ssm_document_name" {
  description = "The name of SSM document to create"
  default     = "add-ssh-key"
}

variable "add_ssha_group_policy" {
  description = "Whether to add a policy to the group that allows SSHA to work"
  default     = false
}

variable "group_name" {
  description = "Group name, optional, for the policy"
  default     = ""
}

variable "required_ec2_tag" {
  description = "Tag key, optional, restricts access to EC2 instances with this tag"
  default     = ""
}

variable "key_cleanup" {
  description = "Enable SSM maintenance task to remove old public keys"
  default     = false
}

variable "key_cleanup_ec2_tag" {
  description = "Tag used to target EC2 instances for public key cleanup"
  default     = "SSHA_Cleanup"
}

variable "user_prefix" {
  description = "Username prefix, optional, enforces usernames to have this prefix before creation"
  default     = ""
}
