{
  "schemaVersion": "2.0",
  "description": "Creates a superuser account with SSH access",
  "parameters": {
    "username": {
      "type": "String",
      "description": "(Required) SSH user name"
    },
    "key": {
      "type": "String",
      "description": "(Required) SSH public key"
    }
  },
  "mainSteps": [
    {
      "action": "aws:runShellScript",
      "name": "runShellScript",
      "inputs": {
        "timeoutSeconds": "30",
        "runCommand": [
          "if [ \"${required_ec2_tag}\" != \"\" ]; then instance_id=$(curl -sS http://169.254.169.254/latest/meta-data/instance-id) region=$(curl -sS http://169.254.169.254/latest/meta-data/placement/availability-zone | sed 's/.$//') found_tag=$(aws ec2 describe-tags --region $region --filters \"Name=resource-id,Values=$instance_id\" \"Name=key,Values=${required_ec2_tag}\" --query 'Tags[0].Key' --output text) && if [ \"$found_tag\" != \"${required_ec2_tag}\" ]; then echo you are not allowed to access this instance; exit 1; fi; fi",
          "if [ \"${user_prefix}\" != \"\" ]; then if [[ \"{{ username }}\" != \"${user_prefix}\"* ]]; then echo please check your user prefix; exit 1; fi; fi",
          "getent group wheel > /dev/null 2>&1 || groupadd wheel",
          "echo '%wheel ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/zzzz_ssm_wheel",
          "chmod 0440 /etc/sudoers.d/zzzz_ssm_wheel",
          "test -d '/home/{{ username }}' || useradd '{{ username }}' --groups wheel",
          "mkdir -p '/home/{{ username }}/.ssh'",
          "touch '/home/{{ username }}/.ssh/authorized_keys'",
          "grep -F '{{ key }}' '/home/{{ username }}/.ssh/authorized_keys' > /dev/null || echo '{{ key }}' >> '/home/{{ username }}/.ssh/authorized_keys'",
          "chown -R '{{ username }}:{{ username }}' '/home/{{ username }}/.ssh'",
          "chmod 700 '/home/{{ username }}/.ssh'",
          "chmod 600 '/home/{{ username }}/.ssh/authorized_keys'",
          "ssh-keyscan localhost"
        ]
      }
    }
  ]
}
