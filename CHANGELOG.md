## 2.0.0 (January 10, 2019)

IMPROVEMENTS:
* Resolves deprecated syntax for Terraform 0.12.14 and later.

## 1.3.0 (August 07, 2017)

IMPROVEMENTS:
* Now prints SSH host keys so SSHA can use them


## 1.2.0 (August 02, 2017)

BUG FIXES:
* Fixed problem with resource count value


## 1.1.0 (July 31, 2017)

IMPROVEMENTS:
* Added support for restricted access
* Updated Readme

BUG FIXES:
* Minor typo fix in readme


## 1.0.0 (July 26, 2017)

Initial version

