data "template_file" "add_ssh_key" {
  template = file("${path.module}/add-ssh-key.json.tpl")

  vars = {
    required_ec2_tag = var.required_ec2_tag
    user_prefix      = var.user_prefix
  }
}

resource "aws_ssm_document" "add_ssh_key" {
  name          = var.ssm_document_name
  document_type = "Command"
  content       = data.template_file.add_ssh_key.rendered
}

resource "aws_ssm_maintenance_window" "key_cleanup" {
  count    = var.key_cleanup ? 1 : 0
  name     = "ssha-key-cleanup"
  schedule = "rate(1 hour)"
  duration = 1
  cutoff   = 0
}

resource "aws_ssm_maintenance_window_target" "key_cleanup" {
  count         = var.key_cleanup ? 1 : 0
  window_id     = join("", aws_ssm_maintenance_window.key_cleanup.*.id)
  resource_type = "INSTANCE"

  targets {
    key    = "tag:${var.key_cleanup_ec2_tag}"
    values = ["true"]
  }
}

resource "aws_ssm_maintenance_window_task" "key_cleanup" {
  count            = var.key_cleanup ? 1 : 0
  window_id        = join("", aws_ssm_maintenance_window.key_cleanup.*.id)
  name             = "ssha-key-cleanup"
  description      = "Removes the authorized keys file"
  task_type        = "RUN_COMMAND"
  task_arn         = "AWS-RunShellScript"
  priority         = 1
  service_role_arn = join("", aws_iam_role.key_cleanup.*.arn)
  max_concurrency  = "2"
  max_errors       = "1"

  targets {
    key    = "WindowTargetIds"
    values = [join("", aws_ssm_maintenance_window_target.key_cleanup.*.id)]
  }

  task_invocation_parameters {
    run_command_parameters {
      parameter {
        name   = "commands"
        values = ["rm -f /home/${var.user_prefix}*/.ssh/authorized_keys"]
      }
    }
  }
}
