data "aws_iam_policy_document" "ssha" {
  count = var.add_ssha_group_policy ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "ec2:DescribeInstances",
      "iam:GetUser",
      "iam:ListGroupsForUser",
      "ssm:DescribeInstanceInformation",
      "ssm:GetCommandInvocation",
    ]

    resources = ["*"]
  }

  statement {
    effect = "Allow"

    actions = [
      "ssm:SendCommand",
    ]

    resources = [
      "arn:aws:ec2:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:instance/*",
      "arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:document/${aws_ssm_document.add_ssh_key.name}",
    ]
  }
}

resource "aws_iam_group_policy" "ssha" {
  count  = var.add_ssha_group_policy ? 1 : 0
  name   = "ssha-${aws_ssm_document.add_ssh_key.name}"
  group  = var.group_name
  policy = join("", data.aws_iam_policy_document.ssha.*.json)
}

resource "aws_iam_role" "key_cleanup" {
  count = var.key_cleanup ? 1 : 0
  name  = "ssha-key-cleanup"
  path  = "/system/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": ["ec2.amazonaws.com","ssm.amazonaws.com"]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

}

resource "aws_iam_role_policy_attachment" "key_cleanup" {
  count      = var.key_cleanup ? 1 : 0
  role       = join("", aws_iam_role.key_cleanup.*.name)
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonSSMMaintenanceWindowRole"
}
